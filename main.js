/*------------------------------
GOALS

/*------------------------------
BUGS

/*------------------------------
Variables globales
*/ 

//canvas layers
var canvasBackground = document.getElementById('canvasBackground');
var ctxBG = canvasBackground.getContext('2d');
var canvasTide = document.getElementById('canvasTide');
var ctxTide = canvasTide.getContext('2d');
var canvasBuild = document.getElementById('canvasBuild');
var ctxBuild = canvasBuild.getContext('2d');
var canvasUI = document.getElementById('canvasUI');
var ctxUI = canvasUI.getContext('2d');
var canvasFront = document.getElementById('canvasFront');
var ctxFront = canvasFront.getContext('2d');
const tideWidth = 700;
const tideX = 250;

//mouse
var canvasPosition = canvasFront.getBoundingClientRect();
const mouse = {
    x: undefined,
    y: undefined,
};
//time
const refresh = 50;
var lastTime = 0;
var timer;
//img
var img =   {
        "Burrow1": null, "Burrow2": null, "Burrow3": null, "Burrow4": null,
            };
var sound = {};
//import/export
var importExport = document.getElementById("importExport");
var infoImportExport = document.getElementById("infoImportExport");
var contentImportExport = document.getElementById("contentImportExport");


/*------------------------------
Classes and algos
*/

/**
 * Generic upgrade class
 * ---------------------
 * methods: 
 * buy() -> void  
 * sliceMyTooltip() -> Array of strings (slice tooltips in lines)  
 */
class Upgrade {
    /**
     * @param {str} name identifier
     * @param {num} cost 
     * @param {num} costAugmentation linear multiplication
     * @param {num} max 1 if unique upgrade
     * @param {bool} now effect now or later
     * @param {str} text front text
     * @param {str} tooltip 
     * @param {str} img 
     */
    constructor(name, cost, costAugmentation, max, now, text, tooltip, img) {
        this.name = name;
        this.cost = cost;
        this.costAugmentation = costAugmentation;
        this.max = max;
        this.current = 0;
        this.visible = false;
        //display when currency attain 0.7x the cost
        this.visibleRatio = 0.7; 
        this.text = text;
        this.tooltip = tooltip;
        this.now = now;
        this.img = img;
    }
    /**
     * add to current, augment cost, change visibility
     */
    buy() {
        this.current++;
        this.cost = Math.round(this.cost*this.costAugmentation);
        this.visible = false;
    }
    /**
     * cut the tooltip in array of strings
     * @returns Array of strings
     */
    sliceMyTooltip() {
        let toolSlice = new Array();
        const maxLength = 45;
        const offset = 15;
        let min = 0;
        let max = min + maxLength-offset;
        while(max < this.tooltip.length) {
            if(this.tooltip[max] === " ") {
                toolSlice.push(this.tooltip.slice(min, max));
                min = max + 1;
                max = min + maxLength - offset;
            } else {
                max++;
            }
        }
        toolSlice.push(this.tooltip.slice(min));
        return toolSlice;
    }
}

/**
 * Generic button class
 * --------------------
 * methods:  
 * clic -> string if clic inside / undefined if clic outside 
 * draw() -> void
 */
class Button {
    /**
     * @param {num} x left
     * @param {num} y top
     * @param {num} w width
     * @param {num} h height
     * @param {bool} clickable if true, show little corners 
     * @param {string} color background color (default yellow)
     * @param {context} ctxParam layer onto we draw (default to front)
     * @param {bool} visible
     * @param {str} effect name of the effect provocked by a clic
     * @param {str} text
     * @param {num} fontSize
     * @param {str} fontColor
     */
    constructor(x,y,w,h,clickable,color,ctxParam, visible, effect, text=undefined, fontSize=0, fontColor="", textX=0, textY=0) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.clickable = clickable;
        this.color = color;
        this.ctx = ctxParam;
        this.visible = visible;
        this.effect = effect;
        this.text = text;
        this.fontSize = fontSize;
        this.fontColor = fontColor;
        this.textX = textX;
        this.textY = textY;
    }
    /**
     * @param {num} x position x of the mouse
     * @param {num} y position y of the mouse
     * @returns {str} if clic reached, else undefined
     */
    clic(x,y) {
        if(this.visible && x >= this.x && x <= this.x+this.w && y > this.y && y < this.y+this.h) {
            return this.effect;
        }
        return undefined;
    }
    draw() {
        if(this.visible) {
            //contour
            this.ctx.lineWidth = 3;
            this.ctx.fillStyle = "rgba(150, 40, 27, 1)";
            this.ctx.strokeStyle = "black";
            this.ctx.fillRect(this.x, this.y, this.w, this.h);
            this.ctx.strokeRect(this.x, this.y, this.w, this.h);
            let rgba;
            switch(this.color) {
                case "red":
                    rgba = "rgba(240, 52, 52, 1)";
                    break;
                case "green":
                    rgba = "rgba(30, 130, 76, 1)";
                    break;
                case "grey":
                    rgba = "rgba(109, 113, 122, 1)";
                    break;
                case "lightgrey":
                    rgba = "rgba(228, 233, 237, 1)";
                    break;
                case "brown":
                    rgba = "rgba(164, 116, 73, 1)";
                    break;
                case "yellow2":
                    rgba = "rgba(245, 215, 110, 1)";
                    break;
                case "yellow":
                default:
                    rgba = "rgba(247, 202, 24, 1)";
                    break;
            }
            this.ctx.fillStyle = rgba;
            this.ctx.fillRect(this.x+5, this.y+5,this.w-10,this.h-10);
            this.ctx.strokeRect(this.x+5, this.y+5,this.w-10,this.h-10);
            if(this.clickable) {
                this.ctx.fillStyle = "rgba(108, 122, 137, 1)";
                this.ctx.lineWidth = 5;
                this.ctx.beginPath();
                this.ctx.arc(this.x+2.5,this.y+2.5,3,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
                this.ctx.beginPath();
                this.ctx.arc(this.x+this.w-2.5,this.y+this.h-2.5,3,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
                this.ctx.beginPath();
                this.ctx.arc(this.x+2.5,this.y+this.h-2.5,3,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
                this.ctx.beginPath();
                this.ctx.arc(this.x+this.w-2.5,this.y+2.5,3,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
            }
            //text
            if(this.text != undefined) {
                this.ctx.font = this.fontSize.toString() + "px Arial";
                this.ctx.fillStyle = this.fontColor;
                this.ctx.fillText(this.text,this.textX+this.x,this.textY+this.y+this.fontSize,this.w-2*this.textX);
            }
        }
    }
}

class PartWave {
    constructor(x,w,h,amp,next=null,prec=null,dir="down") {
        this.x = x;
        this.y = undefined;
        this.blocked = false;
        this.dir = dir;
        this.next = next;
        this.prec = prec;
        this.w = w;
        this.h = h;
        this.amp = amp;
        this.neutral = false;
    }
    unlock(curPos) {
        if(this.blocked) this.amp = -(curPos - this.y);
    }
    destruct() {
        this.next = null;
        this.prec = null;
    }
    draw(curX, regressing) {
        if(!this.neutral) {
            if (regressing) {
                let y = 0;
                if(this.blocked) y = this.y;
                else y = curX+this.amp;
                ctxTide.fillStyle = "rgb(0,0,235)";
                ctxTide.fillRect(tideX+this.x,y,this.w,this.h);
                ctxTide.fillStyle = "rgb(0,0,255)";
                ctxTide.fillRect(tideX+this.x,y+this.h,this.w,this.h);
                ctxTide.fillStyle = "rgb(135,206,250)";
                ctxTide.fillRect(tideX+this.x,y+2*this.h,this.w,this.h);
                ctxTide.fillStyle =  "rgb(240,248,255)";
                ctxTide.fillRect(tideX+this.x,y+3*this.h,this.w,this.h);
                if(Math.random() > 0.9) {
                    ctxTide.clearRect(tideX+this.x,y+4*this.h,this.w,this.h);
                }
            } else {
                let y = 0;
                if(this.blocked) y = this.y;
                else y = curX+this.amp;
                if(!this.prec?.blocked) {
                    ctxTide.fillStyle = "rgb(0,0,245)";
                    if(Math.random() > 0.95) {
                        ctxTide.fillRect(tideX+this.x,y-this.h,this.w,this.h);
                    }
                    ctxTide.fillStyle = "rgb(0,0,255)";
                    ctxTide.fillRect(tideX+this.x,y,this.w,this.h);
                    ctxTide.fillStyle = "rgb(0,0,205)";
                    ctxTide.fillRect(tideX+this.x,y+this.h,this.w,this.h);
                }
                ctxTide.fillStyle = "rgb(0,0,155)";
                ctxTide.fillRect(tideX+this.x,y+2*this.h,this.w,this.h);
            }
        }
    }
}

class Wave {
    constructor() {
        this.currentPos = -20;
        this.parts = null;
        this.partLength = 7;
        this.partWidth = 7;
        this.constructWave();
        this.regressing = false;
    }
    constructWave() {
        let y = 0;
        const amplitude = 10;
        let part1 = null;
        let part2 = null;
        for(let j=0; j<tideWidth; j+=this.partLength) {
            y += Math.floor(Math.random()*5)-2;
            if(y>amplitude) y--;
            if(y<-amplitude) y++;
            part2 = new PartWave(j,this.partLength,this.partWidth,y);
            if(part1 !== null) {
                part1.next = part2;
                part2.prec = part1;
            } else {
                this.parts = part2;
            }
            part1 = part2;
        }
    }
    checkDamage() {
        const sizeTile = 70;
        let builds = tileHandler.askPotentialDamagedBuild(this.currentPos);
        if(builds === undefined) return;
        let posBlocked = []
        for(let i=0; i<builds.length; i++) {
            posBlocked.push({"x1":builds[i].x-tideX,"x2":builds[i].x+builds[i].w-tideX,"y1":builds[i].y,"y2":builds[i].y+sizeTile});
        }
        let part = this.parts;        
        while(part.next !== null) {
            if(part.neutral && part.prec !== null && part.next !== null) {
                if(part.next.x - part.prec.x <= this.partLength) {
                    part.prec.next = part.next;
                    part.next.prec = part.prec;
                } else {
                    let delta = part.prec.amp - part.next.amp;
                    if(delta < 0) delta = 1;
                    else delta = -1;
                    let precLock = false;
                    let nextLock = false;
                    for(let i=0; i<posBlocked.length; i++) {
                        if(precLock && nextLock) break;
                        if(!(part.prec.x + this.partLength < posBlocked[i].x1 ||
                            part.prec.x + this.partLength > posBlocked[i].x2 ||
                            this.currentPos < posBlocked[i].y1 ||
                            this.currentPos > posBlocked[i].y2)
                        ) {
                            precLock = true;                            
                        }
                        if(!(part.next.x - this.partLength < posBlocked[i].x1 ||
                            part.next.x - this.partLength > posBlocked[i].x2 ||
                            this.currentPos < posBlocked[i].y1 ||
                            this.currentPos > posBlocked[i].y2)
                        ) {
                            nextLock = true;                            
                        }
                    }
                    //if(!nextLock || !precLock) console.log("et1",part.prec.prec.x, part.prec.x, part.x, part.next.x, part.next.next.x, nextLock, precLock);
                    if(!precLock) {
                        let newP = new PartWave(part.prec.x+this.partLength,this.partLength,this.partWidth,part.prec.amp+delta,part,part.prec);
                        part.prec.next = newP;
                        part.prec = newP;
                    }
                    if(!nextLock) {
                        let newP = new PartWave(part.next.x-this.partLength,this.partLength,this.partWidth,part.next.amp-delta,part.next,part);
                        part.next.prec = newP;
                        part.next = newP;
                    }
                    //if(!nextLock || !precLock) console.log("et2",part.prec.prec.x, part.prec.x, part.x, part.next.x, part.next.next.x);
                }
            } else if(!part.neutral) {
                let blocked = false;
                for(let i=0; i<posBlocked.length; i++) {
                    if(!(part.x < posBlocked[i].x1 ||
                        part.x > posBlocked[i].x2 ||
                        this.currentPos < posBlocked[i].y1 ||
                        this.currentPos > posBlocked[i].y2)
                    ) {
                        blocked = true;
                        break;
                    }
                }
                if(!part.neutral && blocked) {
                    if(part.prec === null || !part.prec.neutral) {
                        part.neutral = true;
                    } else {
                        part.prec.next = part.next;
                        part.next.prec = part.prec;
                    }
                }
            }
            part = part.next;
        }
    }
    advance(maxPos) {
        this.currentPos+=1;
        if (this.currentPos == maxPos) {
            this.regressing = true;
            /*
            let part = this.parts;
            while(part !== null) {
                part.unlock(this.currentPos);
                part = part.next;
            }*/
        }
        if (this.regressing) {
            this.currentPos-=2;
        } else {
            this.checkDamage();
        }
        return this.regressing && this.currentPos == maxPos-20;
    }
    destruct() {
        let part = this.parts;
        while(part !== null) {
            let part2 = part.next;
            part.destruct();
            part = part2;
        }
    }
    draw() {
        if (Math.floor(this.currentPos) == this.currentPos) {
            let part = this.parts;
            while(part !== null) {
                part.draw(this.currentPos, this.regressing);
                part = part.next;
            }
        }
    }
}

class Tide {
    constructor(currentPos=100) {
        this.currentPos = currentPos;
        this.maxAdvance = 50;
        this.lastTime = lastTime;
        this.timer = 0;
        this.totalTimeToFill = 3600000;  //1h
        this.onePixelTimer = this.totalTimeToFill/canvasTide.height;
        this.waves = new Array();
    }

    advance() {
        if(lastTime != this.lastTime) {
            this.timer += (lastTime - this.lastTime)
            this.lastTime = lastTime;
        }
        if(this.timer >= this.onePixelTimer) {
            this.waves.push(new Wave());
            this.timer = 0;
        }
        let rem = null;
        for(let i=0; i<this.waves.length; i++) {
            if(this.waves[i].advance(this.maxAdvance + this.currentPos)) {
                rem = this.waves[i];
                this.currentPos++;
            }
        }
        if(rem != null) {
            this.waves = this.waves.filter(w => w != rem);
            rem.destruct();
        }
    }

    draw() {
        //ctxTide.clearRect(tideX,0,tideWidth,canvasTide.height);
        //ctxTide.fillStyle = "rgba(0,0,139,1)";
        //ctxTide.fillRect(tideX,0,tideWidth,this.currentPos);
        for(let i=0; i<this.waves.length; i++) {
            this.waves[i].draw();
        }
    }
}

class Tile {
    constructor(x,y,w,h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.building = undefined;
        this.buildImg = undefined;
        this.time = undefined;
        this.totalTime = undefined;
        this.life = undefined;
        this.lifeMax = undefined;
        this.lastTime = undefined;
        this.stage = undefined;
    }
    build(buildName, buildStat) {
        let costs = buildStat.price;
        //TODO: who pays?
        this.life = 1;
        this.lifeMax = buildStat.life;
        this.totalTime = 1000*buildStat.time; //count in ms
        this.time = 0;
        this.building = buildName;
        this.buildImg = img[buildName + "1"];
        this.stage = 1;
        this.lastTime = lastTime;
    }
    advance() {
        if(this.building !== undefined && this.time !== undefined) {
            this.time += lastTime - this.lastTime;
            this.life = Math.floor(this.lifeMax*this.time/this.totalTime);
            this.lastTime = lastTime;
            let stage = 1 + Math.floor((this.time / this.totalTime) * 4);
            if(this.stage != stage) {
                if(stage === 5) {
                    this.time = undefined;
                } else {
                    this.stage = stage;
                    this.buildImg = img[this.building + this.stage.toString()];
                }
            }
        }
    }
    draw(hover=false) {
        if(hover) {
            ctxFront.strokeStyle = "black";
            ctxFront.lineWidth = 5;
            ctxFront.strokeRect(this.x+2.5,this.y+2.5,this.w-5,this.h-5);
        }
        if(this.building !== undefined) {
            ctxBuild.drawImage(this.buildImg, this.x+5, this.y);
            //draw rectangle for life below
            let pc = this.life/this.lifeMax;
            if(pc < 0.25) {
                ctxBuild.fillStyle = "red";
            } else if(pc < 0.5) {
                ctxBuild.fillStyle = "orange";
            } else if(pc < 0.75) {
                ctxBuild.fillStyle = "yellow";
            } else {
                ctxBuild.fillStyle = "green";
            }
            ctxBuild.fillRect(this.x+6,this.y+this.h-9,(this.w-12)*pc,8);
            ctxBuild.strokeStyle = "black";
            ctxBuild.lineWidth = 2;
            ctxBuild.strokeRect(this.x+5,this.y+this.h-10,this.w-10,10);
        }
    }
}

function randInt(a,b) {
    return Math.floor(Math.random()*(b-a+1))+a;
}

function drawBackground() { //why not
    const minR = 230;
    const maxR = 255;
    const minG = 210;
    const maxG = 240;
    const minB = 70;
    const maxB = 120;
    for(let j=0; j<canvasFront.height; j+=8) {
        for(let i=0; i<canvasFront.width; i+=8) {
            let r = randInt(minR,maxR);
            let g = randInt(minG,maxG);
            let b = randInt(minB,maxB);
            ctxBG.fillStyle = "rgba(" + r.toString() + "," + g.toString() + "," + b.toString() + ",1)";
            ctxBG.fillRect(i,j,8,8);
        }
    }
}

/*------------------------------
Handlers
*/

var gameHandler = {
    state: "loading",
    /* list all states possible
       loading
    */
    amIWorking: false, //prevent too much callbacks
    assetsLoaded: 0, //count images loaded
    loading: true,

    initialize: function() {
        if(this.state == "loading") {       
            //loading assets
            this.initializeImg();
            soundHandler.initialize();
        }

        //other handlers
        UIHandler.initialize();
        upgradeHandler.initialize();
        tideHandler.initialize();
        resourceHandler.initialize();
        crabHandler.initialize();
        tileHandler.initialize();

        //loading save
        saveHandler.load();

        drawBackground();

        //launch the loop
        animate(performance.now());
    },
    /**
     * Init all images:  
     * onload call assetsLoading()
     */
    initializeImg: function() {
        let key = Object.keys(img);
        for(let i=0; i<key.length; i++) {
            img[key[i]] = new Image();
            img[key[i]].onload = function() {gameHandler.assetsLoading();};
            img[key[i]].src = "img/"+key[i]+".png";
        }
    },
    assetsLoading: function() {
        this.assetsLoaded++;
    },
    /**
     * @returns bool
     */
    allAssetsLoaded: function() {
        return this.assetsLoaded == (Object.keys(img).length + Object.keys(sound).length);
        //add here other types of assets
    },
    
    draw: function() {
        //clear front
        ctxFront.clearRect(0,0,canvasFront.width,canvasFront.height);

        //call all draw methods as necessary
        tideHandler.draw();
        resourceHandler.draw();
        crabHandler.draw();
        tileHandler.draw();
    },

    core: function() {
        this.amIWorking = true;
        if(this.state === "loading") {
            if(this.allAssetsLoaded()) {
                //this.state = "change here"
                UIHandler.draw();
                this.state = "playing";
            } else {
                //show loading advancement
            }
        } else {
            //do things here
            tideHandler.core();
            resourceHandler.core();
            tileHandler.core();
            crabHandler.core();

            this.draw();
            saveHandler.save();
            upgradeHandler.actualiseVisibility();
        }
        this.amIWorking = false;
    }
};

var upgradeHandler = {
    //contains upgrades objects
    upgradeList: new Array(), 
    //contains names of upgrades that effects later
    upgradeToCome: new Array(), 
    mouseOn: undefined,
    newUpgrade: false,

    /*  list upgrades:

    */

    initialize: function() {
        this.upgradeList = new Array();
        this.upgradeToCome = new Array();
        this.mouseOn = undefined;
        this.newUpgrade = false;
        //to add new Upgrades
        //this.upgradeList.push(new Upgrade(name, cost, costAug, max, now, text, tooltip, img));
    },
    /**
     * recreate list of upgrades after loading (to transform anonym objects from JSON.parse() into Upgrade objects)
     */
    reload: function() {
        let newList = new Array();
        for(let i=0; i<this.upgradeList.length; i++) {
            let u = this.upgradeList[i];
            let u2 = new Upgrade(u.name, u.cost, u.costAugmentation, u.max, u.now, u.text, u.tooltip, u.img);
            u2.current = u.current;
            u2.visible = u.visible;
            u2.visibleRatio = u.visibleRatio;
            newList.push(u2);
        }
        this.upgradeList = newList;
    },
    /**
     * check if enough currency to buy  
     * update currency  
     * effect now or place info for later  
     * @param {Upgrade} upgrade 
     */
    buy: function(upgrade) {
        if(upgrade.cost <= 1 /*gameHandler.money*/) {
            //here change currency handling as fit
            //gameHandler.money -= upgrade.cost;
            upgrade.buy();
            if(upgrade.now)
                this.effect(upgrade.name);
            else   
                this.upgradeToCome.push({name: upgrade.name, text: upgrade.text});
                //here can change pushing method to count {name, text, count}
        }
    },
    /**
     * switch the upgrade names  
     * effect accordingly
     * @param {str} name 
     */
    effect: function(name) {
        switch(name) {
            default:
                break;
        }
    },
    /**
     * effect the upgrades in the upgradeToCome list
     */
    effectUpToCome: function() {
        for(let i=0; i<this.upgradeToCome.length; i++) {
            this.effect(this.upgradeToCome[i].name);
        }
        this.upgradeToCome = new Array();
    },
    /**
     * check all upgrades to see which to display
     * @returns array of Upgrades
     */
    listBuyables: function() {
        return this.upgradeList.filter(e => e.visible);
    },
    /**
     * check the list of upgrades against currency  
     * set this.newUpgrade flag to true if an upgrade becomes visible
     */
    actualiseVisibility: function() {
        for(let i=0; i<this.upgradeList.length; i++) {
            let up = this.upgradeList[i];
            //here change for the currency employed
            let gold = 1; //gameHandler.money;
            if(!up.visible && up.current < up.max && up.cost*up.visibleRatio < gold) {
                up.visible = true;
                //flag that there is a new upgrade in town
                this.newUpgrade = true;
            }
        }
    },

    draw: function() {
        
    },
};

var soundHandler = {
    musicOn: true,

    initialize: function() {
        //start on
        this.musicOn = true;
        this.initializeSound();
    },
    initializeSound: function() {
        /*
        this.sourceMusic = new Audio("sound/music1genericbackgroundtrack.mp3");
        this.sourceMusic.loop = true;
        this.sourceMusic.load();
        this.sourceMusic.addEventListener("load", function() {
            gameHandler.assetsLoading();
        }, true);
        */
    },

    changeHearabilityMusic: function() {
        this.musicOn = !this.musicOn;
        this.actualiseMusic();
    },
    actualiseMusic: function() {
        if(gameHandler.state != "loading") {
            if(this.musicOn) {
                //this.sourceMusic.play();
            } else {
                //this.sourceMusic.pause();
            }
        }
    }
};

var tideHandler = {
    tide: new Tide(),

    initialize() {
        this.tide = new Tide();
    },

    draw() {
        this.tide.draw();
    },
    core() {
        this.tide.advance();
    }
};

var saveHandler = {
    file: undefined,
    countToSave: 500, //how many frames before automatic save
    importOpen: false,

    load: function() {
        this.file = JSON.parse(localStorage.getItem(/* name */"a"));
        if(this.verifySave()) {
            
            //upgrades
            Object.assign(upgradeHandler.upgradeList, this.file.upgrade.upgradeList);
            Object.assign(upgradeHandler.upgradeToCome, this.file.upgrade.upgradeToCome);
            upgradeHandler.newUpgrade = this.file.upgrade.newUpgrade;
            //sound
            soundHandler.musicOn = this.file.sound.musicOn;
            
            upgradeHandler.reload();
        }
        this.createFile();
    },
    save: function() {
        this.countToSave--;
        if(this.countToSave <= 0) {
            this.countToSave = 500;
            this.createFile();
            localStorage.setItem(/* name */"a", JSON.stringify(this.file));
        }
    },
    createFile: function() {
        this.file = {
            "game": {

            },
            "UI": {
                
            },
            "upgrade": {
                "upgradeList": upgradeHandler.upgradeList,
                "upgradeToCome": upgradeHandler.upgradeToCome,
                "newUpgrade": upgradeHandler.newUpgrade,
            },
            "sound": {
                "musicOn": soundHandler.musicOn,
            }
        };
    },
    erase: function() {
        localStorage.removeItem(/* name */"a");

        gameHandler.initialize();
    },
    verifySave: function() {
        return (
            this.file !== undefined && 
            this.file !== null && 
            this.file.game !== undefined && 
            this.file.upgrade !== undefined && 
            this.file.sound !== undefined
            );
    },
    import: function() {
        if(this.importOpen) {
            let save = contentImportExport.value;
            try {
                this.file = JSON.parse(window.atob(save));
                if(this.verifySave()) {
                    localStorage.setItem(/* name */"a", JSON.stringify(this.file));
                    this.countToSave = 500;
                    this.load();
                    this.openImportExport("Save successfully imported");
                }
            } catch(err) {
                contentImportExport.value = err;
            }
        } else {
            this.openImportExport("Import");
            this.importOpen = true;
        }
    },
    export: function() {
        this.createFile();
        this.openImportExport("Copied to clipboard");
        let str = window.btoa(JSON.stringify(this.file));
        contentImportExport.value = str;
        contentImportExport.select();
        document.execCommand("copy");
    },
    openImportExport: function(message) {
        importExport.className = "notHidden";
        infoImportExport.textContent = message;
    },
    closeImportExport: function() {
        importExport.className = "hidden";
        this.importOpen = false;
        contentImportExport.value = "Paste here your code, then click import";
    },

};

var tileHandler = {
    tiles: new Array(),
    size: 70,
    hover: {"x": undefined, "y": undefined},
    commands: new Array(),
    iAmBuilding: false,
    whatAmIBuilding: undefined,
    buildingStats: {},

    initialize() {
        this.buildingStats = {
            "Burrow": { "price": [["sand",10],["crab",10]],
                        "life": 100,
                        "time": 5,
                    },
        };
        this.whatAmIBuilding = undefined;
        this.commands = ["buildBurrow","buildFarm"];
        this.iAmBuilding = false;
        this.tiles = new Array();
        const size = 70;
        this.size = 70;
        this.hover = {"x": undefined, "y": undefined}
        for(let i=0; i<tideWidth; i+=size) {
            let row = new Array();
            for(let j=0; j<tideWidth; j+=size) {
                row.push(new Tile(tideX+j,i,size,size));
            }
            this.tiles.push(row);
        }
    },
    checkHover() {
        let x = mouse.x;
        let y = mouse.y;
        if(x >= tideX && x <= canvasFront.width-tideX) {
            this.hover.y = Math.floor(y/this.size);
            this.hover.x = Math.floor((x-tideX)/this.size);
        } else {
            this.hover.x = undefined;
            this.hover.y = undefined;
        }
    },
    clic(effect) {
        if(effect === "building") { //check if it build something
            if(this.hover.x !== undefined) {
                let tile = this.tiles[this.hover.y][this.hover.x];
                this.tryToBuild(tile);
            }
        } else {  //check if it will try to build
            if(this.commands.includes(effect)) {
                //TODO: check price
                this.iAmBuilding = true;
                this.whatAmIBuilding = effect.substring(5);
                //reinit all colors of others buildings
                for(let i=0; i<this.commands.length; i++) {
                    UIHandler.changeColorOfButton(this.commands[i],"yellow2");
                }
                UIHandler.changeColorOfButton(effect, "green");
            }
        }
    },
    tryToBuild(tile) {
        if(tile.building !== undefined) return false;
        tile.build(this.whatAmIBuilding, this.buildingStats[this.whatAmIBuilding]);
    },
    askPotentialDamagedBuild(pos) {
        if(pos < 0) return undefined;
        let tileList = []
        for(let i=0; i<=Math.floor(pos/this.size); i++) {
            let cur = this.tiles[i].filter(t => t.building !== undefined)
            for(let j=0; j<cur.length; j++) {
                tileList.push(cur[j]);
            }
        }
        return tileList;
    },

    draw() {
        for(let i=0; i<this.tiles.length; i++) {
            for(let j=0; j<this.tiles[i].length; j++) {
                if(this.hover.y == i && this.hover.x == j) {
                    this.tiles[i][j].draw(true);
                } else {
                    this.tiles[i][j].draw();
                }
            }
        }
    },
    core() {
        if(this.iAmBuilding) this.checkHover();
        for(let i=0; i<this.tiles.length; i++) {
            for(let j=0; j<this.tiles[i].length; j++) {
                this.tiles[i][j].advance();
            }
        }
    }
};

var resourceHandler = {
    res: {"algae": [0,0], "sand": [10,0], "crab": [1,0]},
    lastTime: 0,

    initialize() {
        this.res = {"algae": [0,3], "sand": [10,10], "crab": [1,1]};
        this.lastTime = lastTime;
    },
    pay(resName, quantity) {
        if(this.res[resName][0]<quantity) {
            return undefined;
        }
        this.res[resName][0] -= quantity;
        return true;
    },
    addPerSec(resName, quantity) {
        this.res[resName][1] += quantity;
    },
    updatePerSec(resName, quantity) {
        this.res[resName][1] = quantity;
    },
    format(data, ps=false) {
        if(ps) {
            return "(" + Math.floor(data).toString() + "/s)";
        }
        return Math.floor(data).toString();
    },
    draw() {
        ctxFront.fillStyle = "black";
        ctxFront.font = "25px Arial";
        let key = Object.keys(this.res);
        for(let i=0; i<key.length; i++) {
            ctxFront.fillText(this.format(this.res[key[i]][0]),80,200+50*i,68);
            ctxFront.fillText(this.format(this.res[key[i]][1],true),163,200+50*i,68);
        }
    },
    advance() {
        let diff = (lastTime - this.lastTime)/1000;
        this.lastTime = lastTime;
        let key = Object.keys(this.res);
        for(let i=0; i<key.length; i++) {
            this.res[key[i]][0] += diff * this.res[key[i]][1];
        }
    },
    core() {
        this.advance();
    }
};

var crabHandler = {
    commands: new Array(),
    workers: {},

    initialize() {
        this.workers = {"Builder": {"cur": 0, "max": 10, "persec": 1, "produce": "build"}, 
                        "Farmer": {"cur": 0, "max": 10, "persec": 1, "produce": "algae"}, 
                        "Miner":{"cur": 0, "max": 10, "persec": 1, "produce": "sand"}, 
                        "Doctor":{"cur": 0, "max": 10, "persec": 1, "produce": "exp"}
                    };
        let key = Object.keys(this.workers);
        let comm = new Array();
        for(let i=0; i<key.length; i++) {
            comm.push("+" + key[i]);
            comm.push("-" + key[i]);
        }
        this.commands = comm;
    },
    clic(effectName) {
        let effect = effectName[0];
        let work = effectName.substring(1);
        if(effect === "-") {
            this.workers[work].cur--;
            if(this.workers[work].cur < 0) this.workers[work].cur = 0;
        } else {
            if(this.workers[work].cur < this.workers[work].max && resourceHandler.pay("crab",1)) {
                this.workers[work].cur++;
            }
        }
        if(["Farmer","Miner"].includes(work)) {
            resourceHandler.updatePerSec(this.workers[work].produce, this.workers[work].persec * this.workers[work].cur);
        }
    },
    draw() {
        ctxFront.fillStyle = "black";
        ctxFront.font = "25px Arial";
        let key = Object.keys(this.workers);
        for(let i=0; i<key.length; i++) {
            let offY = 380+80*i;
            ctxFront.fillText(key[i] + " :",10,offY,90);
            ctxFront.fillText(this.workers[key[i]].cur,110,offY,40);
            ctxFront.fillText("/",150,offY,10);
            ctxFront.fillText(this.workers[key[i]].max,170,offY,40);
            //ctxFront.fillText(this.format(this.res[key[i]][1],true),163,200+50*i,68);
        }
    },
    core() {

    }
}

var UIHandler = {
    button: new Array(),

    initialize: function() {
        this.button = new Array();
        //this.button.push(new Button(x,y,w,h,clickable,color,ctxParam, visible, effect, text, fontSize, fontColor, textX, textY))
        //here declare all buttons and menus
        this.button.push(new Button(0,0,249,canvasFront.height,false,"grey",ctxUI,true,undefined));
        this.button.push(new Button(canvasFront.width-249,0,249,canvasFront.height,false,"grey",ctxUI,true,undefined));
        //resources
        this.button.push(new Button(0,170,249,45,false,"green",ctxUI,true,undefined));
        this.button.push(new Button(0,220,249,45,false,"yellow2",ctxUI,true,undefined));
        this.button.push(new Button(0,270,249,45,false,"red",ctxUI,true,undefined));
        //crabs
        this.button.push(new Button(0,350,249,40,false,"brown",ctxUI,true,undefined));
        this.button.push(new Button(0,430,249,40,false,"green",ctxUI,true,undefined));
        this.button.push(new Button(0,510,249,40,false,"yellow2",ctxUI,true,undefined));
        this.button.push(new Button(0,590,249,40,false,"red",ctxUI,true,undefined));
        //crab buttons
        this.button.push(new Button(120,395,30,30,true,"red",ctxUI,true,"-Builder","-",35,"black",9,-11));
        this.button.push(new Button(160,395,30,30,true,"green",ctxUI,true,"+Builder","+",37,"black",4,-9));
        this.button.push(new Button(120,475,30,30,true,"red",ctxUI,true,"-Farmer","-",35,"black",9,-11));
        this.button.push(new Button(160,475,30,30,true,"green",ctxUI,true,"+Farmer","+",37,"black",4,-9));
        this.button.push(new Button(120,555,30,30,true,"red",ctxUI,true,"-Miner","-",35,"black",9,-11));
        this.button.push(new Button(160,555,30,30,true,"green",ctxUI,true,"+Miner","+",37,"black",4,-9));
        this.button.push(new Button(120,635,30,30,true,"red",ctxUI,true,"-Doctor","-",35,"black",9,-11));
        this.button.push(new Button(160,635,30,30,true,"green",ctxUI,true,"+Doctor","+",37,"black",4,-9));
        //buildings
        this.button.push(new Button(961,10,110,110,true,"yellow2",ctxUI,true,"buildBurrow","burrow",20,"black",25,5));
        this.button.push(new Button(1079,10,110,110,true,"yellow2",ctxUI,true,"buildFarm","farm",20,"black",32,5));
    },
    changeColorOfButton(effect, color) {
        for(let i=0; i<this.button.length; i++) {
            if(this.button[i].effect == effect) {
                this.button[i].color = color;
                this.button[i].draw();
                return true;
            }
        }
        return false;
    },
    clic: function() {
        let x = mouse.x;
        let y = mouse.y;
        let i=0;
        //to stop when a clic is performed
        let clicEffect = false;
        if(tileHandler.iAmBuilding) {
            if(tileHandler.clic("building")) {
                clicEffect = true;
            }
        }
        while(i<this.button.length && !clicEffect) {
            let effectName = this.button[i].clic(x,y);
            if(effectName !== undefined) {
                //here effect the clic
                if(crabHandler.commands.includes(effectName)) {
                    crabHandler.clic(effectName);
                } else if(tileHandler.commands.includes(effectName)) {
                    tileHandler.clic(effectName);
                }
                clicEffect = true;
            }
            i++;
        }
    },
    draw: function() {
        //TODO : no buttons at the time 
        for(let i=0; i<this.button.length; i++) {
            this.button[i].draw();
        }
        
    },
};

/*------------------------------
Interaction utilisateur
*/

document.addEventListener('mousemove', e => {
    canvasPosition = canvasFront.getBoundingClientRect();
    mouse.x = Math.round(e.x - canvasPosition.left);
    mouse.y = Math.round(e.y - canvasPosition.top);
    //what to do with that?
});

document.addEventListener('mousedown', e => {mouseDown()});
document.addEventListener('mouseup', e => {mouseUp()});

function mouseDown() {
    UIHandler.clic();
}

function mouseUp() {

}

/**
 * main loop, launch this.core() each time
 * @param {time} timestamp 
 */
function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
        if(!gameHandler.amIWorking) {
            //console.time("core");
            gameHandler.core();
            //console.timeEnd("core");
            timer += timestamp - lastTime;
            lastTime = timestamp;
        }
    }
    window.requestAnimationFrame(animate);
}

/*------------------------------
Démarrage de la page
*/

window.onload = function() {
    gameHandler.initialize();
}